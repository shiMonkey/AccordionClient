﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccordionDemo
{
    public class DataSourceClass
    {
        /// <summary>
        /// 数据源
        /// </summary>
        public static List<ExpanderClass> GetDateSource()
        {
            List<ExpanderClass> exLst = new List<ExpanderClass>();
            List<ImgUrlClass> lst = new List<ImgUrlClass>();
            for (int i = 1; i < 10; i++)
            {
                lst.Add(new ImgUrlClass() { ImageUrl = string.Format("Images/h{0}.png", i) });
            }
            exLst.Add(new ExpanderClass()
            {
                Title = "我是第一行哦！",
                ImgUrl = "Images/Left.png",
                ImgLst = lst
            });

            lst = new List<ImgUrlClass>();
            for (int i = 1; i < 10; i++)
            {
                lst.Add(new ImgUrlClass() { ImageUrl = string.Format("Images/h1{0}.png", i) });
            }
            exLst.Add(new ExpanderClass()
            {
                Title = "我是第二行哦！！",
                ImgUrl = "Images/Right.png",
                ImgLst = lst
            });

            lst = new List<ImgUrlClass>();
            for (int i = 1; i < 10; i++)
            {
                lst.Add(new ImgUrlClass() { ImageUrl = "Images/h10.png" });
            }
            exLst.Add(new ExpanderClass()
            {
                Title = "我是第三行哦！！！",
                ImgUrl = "Images/Up.png",
                ImgLst = lst
            });
            return exLst;
        }
    }

    public class ExpanderClass
    {
        public string Title { get; set; }

        public string ImgUrl { get; set; }

        public List<ImgUrlClass> ImgLst { get; set; }

        public ExpanderClass()
        {
            Title = string.Empty;
            ImgUrl = string.Empty;
        }
    }

    public class ImgUrlClass
    {
        public ImgUrlClass()
        {
            ImageUrl = string.Empty; 
        }

        public string ImageUrl { get; set; } 
    }
}
