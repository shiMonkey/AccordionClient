﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccordionDemo
{
    /// <summary>
    /// AccordionControl.xaml 的交互逻辑
    /// </summary>
    public partial class AccordionControl : UserControl
    {
        public AccordionControl()
        {
            InitializeComponent();
        }

        #region 成员变量

        private RadioButton rdoImage = null;

        #endregion

        #region 事件处理

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = DataSourceClass.GetDateSource();
        }

        private void rdoImg_Click(object sender, RoutedEventArgs e)
        {
            rdoImage = e.OriginalSource as RadioButton;
            gridMaskLayer.Visibility = Visibility.Visible;
        }

        private void gridMaskLayer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            gridMaskLayer.Visibility = Visibility.Collapsed;
            rdoImage.IsChecked = false;
        }

        #endregion


    }
}
